{
  "color": color,
  "name": name,
  "sellQuantity": sellQuantity,
  "description": description,
  "weight": weight,
  "id": id,
  "type": type,
  "shelfLife": shelfLife,
  "brand": brand
}